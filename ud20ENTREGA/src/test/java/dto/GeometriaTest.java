package dto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GeometriaTest {

	Geometria geo;
	
	@BeforeEach
	public void before() {
		geo = new Geometria();
	}
	@Test
	void testAreacuadrado() {
		int resultado = geo.areacuadrado(2);
		int esperado = 4;
		assertEquals(esperado, resultado);
	}

	@Test
	void testAreaCirculo() {
		double resultado = geo.areaCirculo(4);
		double esperado = 50.2656;
		assertEquals(esperado, resultado);
		
	}

	@Test
	void testAreatriangulo() {
		double resultado = geo.areatriangulo(2, 3);
		double esperado = 3;
		assertEquals(esperado, resultado);
	}

	@Test
	void testArearectangulo() {
		double resultado = geo.arearectangulo(2, 3);
		double esperado = 6;
		assertEquals(esperado, resultado);
	}

	@Test
	void testAreapentagono() {
		double resultado = geo.areapentagono(2, 3);
		double esperado = 3;
		assertEquals(esperado, resultado);
	}

	@Test
	void testArearombo() {
		double resultado = geo.arearombo(2, 3);
		double esperado = 3;
		assertEquals(esperado, resultado);
	}

	@Test
	void testArearomboide() {
		double resultado = geo.arearomboide(2, 3);
		double esperado = 6;
		assertEquals(esperado, resultado);
	}

	@Test
	void testAreatrapecio() {
		double resultado = geo.areatrapecio(2, 2, 2);
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testGetId() {
		geo = new Geometria(3);
		int resultado = geo.getId();
		int esperado = 3;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testFigura() {
		String resultado = geo.figura(3);
		String esperado = "Triangulo";
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testSetId() {
		geo.setId(5);
		int resultado = geo.getId();
		int esperado = 5;
		assertEquals(esperado, resultado);
	}

	@Test
	void setGetNom() {
		geo.setNom("Triangle");
		String resultado = geo.getNom();
		String esperado = "Triangle";
		assertEquals(esperado, resultado);
	}

}
